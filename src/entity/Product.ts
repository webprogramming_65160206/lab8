import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToMany } from "typeorm"
import { Type } from "./Type"
import { OrderItem } from "./OrderItem"

@Entity()
export class Product {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    price: number

    @CreateDateColumn()
    created: Date

    @UpdateDateColumn()
    updated: Date

    // มันจะเชื่อมโยง อย่างแรกดูว่าเป็น many to one or one to many แล้วอีกฝั้งจะตามมาเอง อย่าลืมtype
    @ManyToOne(() => Type, (type) => type.products)
    type: Type;

    //ถ้าเป็น one to many มี attribute ต้องมี s
    @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
    orderItems: OrderItem[];

}

import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Order } from "./Order";
import { Product } from "./Product";

@Entity()
export class OrderItem {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    price: number

    @Column()
    qty: number

    @Column()
    total: number

    @CreateDateColumn()
    created: Date

    @UpdateDateColumn()
    updated: Date

    // มองเป็นกลับกันว่ามันโยงไปที่ไหนแล้วมีได้กี่ตัว นึกถึงหน้าจารย์โกเมศ แล้วก็เขียนเลย
    @ManyToOne(() => Order, (order) => order.orderItems)
    order: Order;

    @ManyToOne(() => Product, (product) => product.orderItems)
    product: Product;


}
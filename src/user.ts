import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {
    const usersRepository = AppDataSource.getRepository(User)
    const rolesRepository = AppDataSource.getRepository(Role)

    const adminRole = await rolesRepository.findOneBy({ id: 1 })
    const userRole = await rolesRepository.findOneBy({ id: 2 })
    usersRepository.clear()
    console.log("Inserting a new user into the Memory...")
    var user = new User()
    user.id = 1
    user.email = "admin@email.com"
    user.gender = "male"
    user.password = "Pass@1234"
    user.roles = [];
    user.roles.push(adminRole)
    user.roles.push(userRole)
    console.log("Inserting a new user into the Database...")
    await usersRepository.save(user);


    user = new User()
    user.id = 2
    user.email = "user1@email.com"
    user.gender = "male"
    user.password = "Pass@1234"
    user.roles = [];
    user.roles.push(adminRole)
    user.roles.push(userRole)
    console.log("Inserting a new user into the Database...")
    await usersRepository.save(user);


    user = new User()
    user.id = 3
    user.email = "user2@email.com"
    user.gender = "female"
    user.password = "Pass@1234"
    user.roles = []
    user.roles.push(userRole)
    console.log("Inserting a new user into the Database...")
    await usersRepository.save(user);

    const users = await usersRepository.find({ relations: { roles: true } })
    // JSON.stringify(users) จะทำให้แสดงเป็น string จะเห็นหมด ใส่ null กับสองtab
    console.log(JSON.stringify(users, null, 3))
    // การใส่relation จะreturn ตัวที่ reference many to many กันด้วย
    const roles = await rolesRepository.find({ relations: { users: true } })
    console.log(JSON.stringify(roles, null, 3))
    //ถ้าทำไม่ได้ให้ไปดูเว็บ typeorm ให้ครบถ้วน นะจ๊ะ

}).catch(error => console.log(error))
